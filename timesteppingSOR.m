function [t,q,u] = timesteppingSOR(sys_file,tspan,q0,u0,Ni,rtol,atol,maxiter)
% Moreau's Timestepping Scheme with SOR-prox method for planar problems
% 
% usage: [t,q,u] = timesteppingSOR(sys_file,tspan,q0,u0,Ni,tol,maxiter);
%
% input:  sys_file  system file
%         tspan     [t_begin,t_end]
%         q0,u0     inital condition 
%         Ni        number of discretisation points
%         rtol      relative tolerance for the fixed point iteration
%         atol      absolute tolerance for the fixed point iteration
%         maxiter   maximal number of iterations
%
% output: t         time
%         q         position
%         u         velocity
%
% Remco Leine August 2011, ETH Zurich

t0 = tspan(1); te = tspan(2);

if nargin<6, rtol = 1e-7; end
if nargin<7, atol = 1e-12; end
if nargin<8, maxiter = 100; end

[isconstant_M,isconstant_contactpar,alpha_r] = feval(sys_file,0,0,0,'init'); %load variables
[muF,epsilonNF,epsilonTF] =feval(sys_file,t0,q0,u0,'contactpar');
MM =feval(sys_file,t0,q0,u0,'M');
nG =length(feval(sys_file,t0,q0,u0,'gap')); %number of contacts
ndof = length(MM); %number of degrees of freedom

dt = (te-t0)/(Ni-1);
t = t0:dt:te;

q = zeros(ndof,Ni);
u = zeros(ndof,Ni);
q(:,1) = q0;
u(:,1) = u0;
PN = zeros(nG,1);
PT = zeros(nG,1);

for i=1:(Ni-1);
 if rem(i,1000)==0, disp(['i = ',num2str(i)]);end

 %start integration step
 qA = q(:,i);
 uA = u(:,i);
 tA = t(i);
 
 %calculate midpoint
 qM = qA + dt/2*uA;
 tM = tA+dt/2;
 hM =feval(sys_file,tM,qM,uA,'h');
 if ~isconstant_M,MM =feval(sys_file,tM,qM,uA,'M'); end;
 
 %calculate index set
 gNM = feval(sys_file,tM,qM,uA,'gap');         
 I_N = find(gNM<=0)';
 I_Ncomp =  find(gNM>0)';
 nN = length(I_N);
 
 %solve contact problem and end integration step
 if nN>0
  [WN,WT,wNhat,wThat] = feval(sys_file,tM,qM,uA,'Wmat');
  WN = WN(:,I_N);
  WT = WT(:,I_N);
  PN_s = PN(I_N);
  PT_s = PT(I_N);
  if ~isconstant_contactpar, [muF,epsilonNF,epsilonTF] =feval(sys_file,tM,qM,uA,'contactpar'); end
  mu = muF(I_N);
  GNN = WN'*(MM\WN);
  GNT = WN'*(MM\WT);
  GTN = WT'*(MM\WN);
  GTT = WT'*(MM\WT);
  rN = alpha_r ./ diag(GNN);
  rT = alpha_r ./ diag(GTT);
  gammaNA = WN'*uA + wNhat(I_N);
  gammaTA = WT'*uA + wThat(I_N);
  epsilonN = epsilonNF(I_N);
  epsilonT = epsilonTF(I_N);
  cN = (1 + epsilonN).*gammaNA + WN'*(MM\hM*dt);
  cT = (1 + epsilonT).*gammaTA + WT'*(MM\hM*dt);
  converged=0;
  iter=0;
  
  while (~converged && iter<maxiter) %Gauss Seidel fixed point iteration 
   PN_old = PN_s;
   PT_old = PT_s;
   for j=1:nN
    PN_s(j) = proxCN(PN_s(j) - rN(j)*(GNN(j,:)*PN_s + GNT(j,:)*PT_s + cN(j)));
    PT_s(j) = proxCT(PT_s(j) - rT(j)*(GTN(j,:)*PN_s + GTT(j,:)*PT_s + cT(j)),mu(j)*PN_s(j));
   end
   errorN = max(abs(PN_old-PN_s)-abs(PN_old)*rtol); 
   errorT = max(abs(PT_old-PT_s)-abs(PT_old)*rtol); 
   error = max(errorN,errorT);
   converged = error< atol;
   iter = iter+1;
  end
  uE = uA  + MM\(hM*dt + WN*PN_s + WT*PT_s);
  PN(I_N) = PN_s; %put the contact efforts of the closed contacts in the vector of all contact efforts
  PT(I_N) = PT_s;
  PN(I_Ncomp) = zeros(nG-nN,1);
  PT(I_Ncomp) = zeros(nG-nN,1);
 else  %if all constacts are open
  uE = uA  + MM\(hM*dt);
  converged = 1;
  error = 0;
  PN = zeros(nG,1);
  PT = zeros(nG,1);
 end

 if converged == 0 && error>5*rtol, i, error, converged, end;
 qE = qM+ uE*dt/2;

 %update
 q(:,i+1) = qE;
 u(:,i+1) = uE;

end


%--------------------------------------------------------------------------
function y = proxCN(x)
%proximal point function for CN = R^+
y = max(x,0);

%--------------------------------------------------------------------------
function y = proxCT(x,a)
%proximal point function for CT = [-a,a]
y = max(min(x,a),-a);        